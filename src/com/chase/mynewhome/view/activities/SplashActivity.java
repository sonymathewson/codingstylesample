package com.chase.mynewhome.view.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;

import com.chase.core.util.PreferenceManager;
import com.chase.mynewhome.R;
import com.chase.mynewhome.app.OpenHouseApplication;
import com.chase.mynewhome.db.FavoritesDAO;
import com.chase.mynewhome.util.AnalyticsUtility;
import com.chase.mynewhome.util.ConfigFile;
import com.chase.mynewhome.util.Keys;
import com.chase.mynewhome.view.activities.firstrun.LegalAgreementActivity;
import com.chase.mynewhome.view.activities.firstrun.QuickSearchActivity;
import com.chase.mynewhome.view.activities.search.SearchActivity;
import com.chase.mynewhome.view.appwidget.OpenHouseService;

public class SplashActivity extends Activity {

    private static final int SPLASH_DELAY_FIRST_RUN = 2000;
    private static final int SPLASH_DELAY_SECOND_RUN = 1000;
    private Handler handler;
    private Runnable delayRunner;
    private String pageName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        intializeUI();
    }

    private void intializeUI() {
        try {
            PreferenceManager.put(this, Keys.PreferenceKeys.APP_VERSION_NAME, getPackageManager()
                    .getPackageInfo(getPackageName(), 0).versionName);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        String trackEvent = getString(R.string.Screen_Load);
        pageName = getString(R.string.Welcome_View);
        AnalyticsUtility.onPageLoad(pageName, trackEvent);
        OpenHouseApplication.getApp().setUpgradeCheck(true);
        handler = new Handler();
        delayRunner = new Runnable() {

            @Override
            public void run() {
                Intent intent;
                /** get the current DB vesrion from configfile **/
                int dbVersion = Integer
                        .parseInt(ConfigFile.getDatabaseVersion(SplashActivity.this));
                /**
                 * get the oldDbVersion from the onUpgrade set and compare with current dbversion
                 **/
                int oldDbVersion = PreferenceManager.getInt(SplashActivity.this,
                        Keys.PreferenceKeys.PREF_OLD_DB_VERSION);
                if (oldDbVersion == 1 || oldDbVersion ==2 || oldDbVersion ==3
                        || ((dbVersion > oldDbVersion) && (ConfigFile
                                .isLegalAggreementChanged(SplashActivity.this)
                                .equalsIgnoreCase("true")))) {
                    intent = new Intent(SplashActivity.this, LegalAgreementActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    if (isLegalTermsAccepted()) {
                    		if (isPersonalSettingsSaved()) {
                    			intent = new Intent(SplashActivity.this, SearchActivity.class);
                                //SearchActivity.invalidateList(true);
                            } else {
                    			intent = new Intent(SplashActivity.this, QuickSearchActivity.class);
                            }                    		
                    } else {
                    	PreferenceManager.put(SplashActivity.this,
                                Keys.PreferenceKeys.PREF_SHOW_APPLICATION_TUTORIAL, true);
                        intent = new Intent(SplashActivity.this, LegalAgreementActivity.class);
                    }
                    startActivity(intent);
                    finish();
                }
            }
        };
        if (isLegalTermsAccepted()) {
            handler.postDelayed(delayRunner, SPLASH_DELAY_SECOND_RUN);
        } else {
            handler.postDelayed(delayRunner, SPLASH_DELAY_FIRST_RUN);
        }
        initiateService();
    }

    private void initiateService() {
        if (!FavoritesDAO.fetchListingKeys().isEmpty()) {
            startService(new Intent(this, OpenHouseService.class));
        }
    }

    private boolean isLegalTermsAccepted() {
        return PreferenceManager.getBoolean(this, Keys.PreferenceKeys.ACCEPT_LEGAL);
    }

    private boolean isPersonalSettingsSaved() {
        return PreferenceManager.getBoolean(this, Keys.PreferenceKeys.PERSONAL_SETTINGS);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        handler.removeCallbacks(delayRunner);
    }
}
