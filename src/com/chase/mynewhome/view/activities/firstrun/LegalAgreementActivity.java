package com.chase.mynewhome.view.activities.firstrun;

import java.io.IOException;
import java.io.InputStream;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chase.core.model.Response;
import com.chase.core.util.InputStreamUtil;
import com.chase.core.util.OpenHouseLog;
import com.chase.core.util.PreferenceManager;
import com.chase.mynewhome.R;
import com.chase.mynewhome.actions.ServiceActions;
import com.chase.mynewhome.app.OpenHouseApplication;
import com.chase.mynewhome.model.ForceUpgradeResponseData;
import com.chase.mynewhome.util.AnalyticsUtility;
import com.chase.mynewhome.util.AppConstants;
import com.chase.mynewhome.util.ConfigFile;
import com.chase.mynewhome.util.Keys;
import com.chase.mynewhome.view.activities.application.tutorial.ApplicationTutorialActivity;
import com.chase.mynewhome.view.activities.application.tutorial.ApplicationTutorialManager;
import com.chase.mynewhome.view.activities.firstrun.helper.SearchViewManager;
import com.chase.mynewhome.view.activities.search.SearchActivity;

/**
 * This Activity is shown every time, till the user doesn't Accept the chase legal agreement
 */
public class LegalAgreementActivity extends Activity implements OnClickListener {

    private static final String TAG = "LegalAgreementActivity";
    private String trackEvent;
    private String pageName;
    private SearchViewManager searchViewManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.legal_agreement);
        initializeUI();
        Intent intent = getIntent();
        if (intent.hasExtra(AppConstants.INTENT_VIEW_EULA)) {
            TextView agreementInstruction = (TextView)findViewById(R.id.agreement_instruction);
            LinearLayout buttonLayout = (LinearLayout)findViewById(R.id.button_layout);
            agreementInstruction.setVisibility(View.GONE);
            buttonLayout.setVisibility(View.GONE);
        }
    }

    private void initializeUI() {
        ((Button)findViewById(R.id.accept_btn)).setOnClickListener(this);
        ((Button)findViewById(R.id.decline_btn)).setOnClickListener(this);
        setData();
        trackEvent = getString(R.string.Screen_Load);
        pageName = getString(R.string.First_Legal_Agreement_View);
        AnalyticsUtility.onPageLoad(pageName, trackEvent);
        searchViewManager = new SearchViewManager(this);
        if (OpenHouseApplication.getApp().isUpgradeCheck()) {
            OpenHouseApplication.getApp().setUpgradeCheck(false);
            // updateCheck();
            searchViewManager.setForceUpgradeEvent();
        }
    }

    private void setData() {
        TextView agreementContent = (TextView)findViewById(R.id.agreement_content);
        InputStream inputStream = getResources().openRawResource(R.raw.legal_agreement);
        String content = "";
        try {
            content = InputStreamUtil.inputStreamToString(inputStream);
        } catch (IOException e) {
            OpenHouseLog.e(TAG, e.getMessage());
        }
        agreementContent.setText(Html.fromHtml(content));
        agreementContent.setMovementMethod(LinkMovementMethod.getInstance());
    }
    
    public void setData(Response<?> response) {
    	if (response.getAction() == ServiceActions.FORCE_UPGRADE) {
            ForceUpgradeResponseData forceUpgradeResponseData = new ForceUpgradeResponseData();
            forceUpgradeResponseData = ((ForceUpgradeResponseData)response.getData());
            if (forceUpgradeResponseData != null
                    && forceUpgradeResponseData.getAppStoreUpdateUrl() != null) {
                if (forceUpgradeResponseData.getUpdateIsMandatory().equalsIgnoreCase("true")
                        || forceUpgradeResponseData.getOperatingSystemNotSupported()
                                .equalsIgnoreCase("true")
                        || forceUpgradeResponseData.getAppStoreUpdateUrl().length() >= 5) {
                    showUpdateDialog(forceUpgradeResponseData.getUpdateMessage(),
                            forceUpgradeResponseData.getAppStoreUpdateUrl(),
                            forceUpgradeResponseData.getOperatingSystemNotSupported(),
                            forceUpgradeResponseData.getUpdateIsMandatory());
                }
            }
        }
    }
    
	public void showUpdateDialog(String updateMessageInfo,
			final String googlePlayUrlLink, final String osSupportCheck,
			String updateMandatoryCheck) {
		AlertDialog.Builder mBuilder = new AlertDialog.Builder(
				LegalAgreementActivity.this);
		mBuilder.setMessage(updateMessageInfo);
		mBuilder.setTitle("Update Available");
		mBuilder.setCancelable(false);
		// googlePlayUrl = googlePlayUrlLink;
		String NegativeButtonText = "Update";
		if (osSupportCheck.equalsIgnoreCase("true"))
		{
			NegativeButtonText = "Ok";
			mBuilder.setTitle("Operating System version not supported");
		}
		mBuilder.setNegativeButton(NegativeButtonText,
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int whichButton) {
						// Uri uri =
						// Uri.parse("market://details?id=com.chase.mynewhome");
						if (osSupportCheck.equalsIgnoreCase("false")) {
							if (!TextUtils.isEmpty(googlePlayUrlLink)) {
								Uri uri = Uri.parse(googlePlayUrlLink);
								Intent intent = new Intent(Intent.ACTION_VIEW,
										uri);
								startActivity(intent);
								finish();
							}
						}
						else
							finish();
					}
				});
		// if (TextUtils.isEmpty(googlePlayUrlLink))
		if (updateMandatoryCheck.equalsIgnoreCase("false")
				&& osSupportCheck.equalsIgnoreCase("false")) {
			mBuilder.setPositiveButton("Not now",
					new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog,
								int whichButton) {
						}
					});
		}
		mBuilder.create();
		AlertDialog dialog = mBuilder.show();
		// Must call show() prior to fetching text view
		TextView messageView = (TextView) dialog
				.findViewById(android.R.id.message);
		// messageView.setGravity(Gravity.CENTER);
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.decline_btn:
            	AnalyticsUtility.onPageLoad(getString(R.string.First_Legal_Agreement_View),
            			getString(R.string.Button_Pressed), getString(R.string.Button_Legal_Agreement_View_Decline),
            			getString(R.string.Yes), AppConstants.INT_ZERO);
                Intent intentDeclined = new Intent(LegalAgreementActivity.this,
                        DeclinedPromoActivity.class);
                startActivity(intentDeclined);
                break;
            case R.id.accept_btn:
            	AnalyticsUtility.onPageLoad(getString(R.string.First_Legal_Agreement_View),
            			getString(R.string.Button_Pressed), getString(R.string.Button_Legal_Agreement_View_Accept),
            			getString(R.string.Yes), AppConstants.INT_ZERO);
                PreferenceManager.put(this, Keys.PreferenceKeys.PREF_OLD_DB_VERSION,
                        Integer.parseInt(ConfigFile.getDatabaseVersion(this)));
                Intent intentAgreed;
                legalTermsAccepted();
                ApplicationTutorialManager appTutorialManager = new ApplicationTutorialManager();
                appTutorialManager.insertIntialValues();
              
                if (isPersonalSettingsSaved()) {
                    // TODO: Remove tab
                    /*
                     * intentAgreed = new Intent(LegalAgreementActivity.this,
                     * NavigatorActivity.class);
                     */
                    intentAgreed = new Intent(LegalAgreementActivity.this, SearchActivity.class);
                } else {
                    intentAgreed = new Intent(LegalAgreementActivity.this,
                    		ApplicationTutorialActivity.class);
                    intentAgreed.putExtra(AppConstants.INTENT_EXTRA_QUICKLINKS_KEY, true);
                }
                startActivity(intentAgreed);
                finish();
                break;
            default:
                break;
        }
    }

    private void legalTermsAccepted() {
        PreferenceManager.put(this, Keys.PreferenceKeys.ACCEPT_LEGAL, true);
    }

    private boolean isPersonalSettingsSaved() {
        return PreferenceManager.getBoolean(this, Keys.PreferenceKeys.PERSONAL_SETTINGS);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Page to be tracked on load
        AnalyticsUtility.onPageLoad(pageName, trackEvent);
    }


}
