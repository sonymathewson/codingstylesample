package com.chase.mynewhome.view.activities.firstrun;

import android.app.Activity;
import android.os.Bundle;
import com.chase.mynewhome.R;
import com.chase.mynewhome.util.AnalyticsUtility;

/**
 * This Promo Activity is triggered/shown when the LegalAgreement is Declined
 */
public class DeclinedPromoActivity extends Activity {
	private String trackEvent;
    private String pageName;
   
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	setContentView(R.layout.declined_promo_screen);
    	trackEvent = getString(R.string.Button_Pressed);
        pageName = getString(R.string.Declined_Promo);
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	 AnalyticsUtility.onPageLoad(pageName, trackEvent);
    }
}
