/**
 * 
 */
package com.chase.mynewhome.view.activities.firstrun;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import com.chase.core.ResponseType;
import com.chase.core.activity.BaseActivity;
import com.chase.core.model.Response;
import com.chase.core.util.PreferenceManager;
import com.chase.core.util.bitmaputil.PixelSize;
import com.chase.mynewhome.R;
import com.chase.mynewhome.actions.ServiceActions;
import com.chase.mynewhome.app.OpenHouseApplication;
import com.chase.mynewhome.db.SearchLocationHistoryDAO;
import com.chase.mynewhome.model.HomeSetupData;
import com.chase.mynewhome.model.listing.AreaLocation;
import com.chase.mynewhome.model.listing.ListingSummaryRequestData;
import com.chase.mynewhome.util.AppConstants;
import com.chase.mynewhome.util.Keys;
import com.chase.mynewhome.util.Utility;
import com.chase.mynewhome.view.activities.firstrun.helper.SearchViewManager;
import com.chase.mynewhome.view.activities.search.HistoryListAdapter;
import com.chase.mynewhome.view.activities.search.SearchActivity;
import com.chase.mynewhome.view.activities.search.SelectLocationActivity;
import com.chase.mynewhome.view.activities.util.HomeSetupUtil;
import com.chase.mynewhome.view.activities.util.SearchHelper;
import com.chase.mynewhome.view.controls.OHErrorDialog;
import com.chase.mynewhome.view.controls.PageHeader;
import com.chase.mynewhome.view.controls.SearchBar;
import com.chase.mynewhome.view.controls.SearchWordWatcher;
import com.chase.mynewhome.view.controls.SearchWordWatcher.TextExtractListener;
import com.chase.mynewhome.view.controls.SegmentedControlBar;
import com.chase.mynewhome.view.controls.SegmentedControlBar.SegmentedButtonListener;
import com.chase.mynewhome.view.controls.SingleChoiceDialogBuilder;
import com.chase.mynewhome.view.controls.SingleChoiceDialogBuilder.DialogActionListner;
import com.chase.mynewhome.view.controls.TransparentListView;
import com.chase.mynewhome.view.controls.choicepicker.MultiChoiceDialog;
import com.chase.mynewhome.view.controls.choicepicker.MultiChoiceDialog.MultiChoiceListner;
import com.chase.mynewhome.view.controls.rangepicker.PriceRangePickerDialog;
import com.chase.mynewhome.view.controls.rangepicker.PriceRangePickerDialog.OnPriceSetListener;
import com.chase.mynewhome.view.controls.rangepicker.PriceRangePickerDialog.ValidationCallback;

public class QuickSearchActivity extends BaseActivity implements OnItemClickListener,SegmentedButtonListener
         {

    private static final int STATE_ZIP_VIEW = 0;
    private static final int MLS_VIEW = 1;
    private int currentView = STATE_ZIP_VIEW;
    private String searchKeyword;
    // VO
    private HomeSetupData homeSetupData = new HomeSetupData();
    private SearchViewManager searchViewManager;
    private ListingSummaryRequestData listingSummaryRequestData = new ListingSummaryRequestData();
    // Views
    private ViewSwitcher settingsSwitcher;
    private PageHeader header;
    private TextView priceRangeValue;
    private TextView homeTypeValue;
    private EditText locationBox;
    private EditText mlsidEditText;
    private Button searchButton;
    private List<String> previousSearchedMLSIDs;
    private ImageButton mlsClearButton;
    private HistoryListAdapter historyListAdapter;
    private TransparentListView locationList;
    private String mSelectedBedRoomValue;
    private TextView mBedRoomTextView;
    private TextView mBathRoomTextView;
    private TextView mSquareFootageTextView;
    private TextView mSearchRadiusTextView;
    private ImageView dialogIcon;
    private TextView dialogTxtMessage;
    private String mSelectedBathRoomValue;
    private String mSelectedSquareFootageValue;
    private String mSelectedSearchRadiusValue;
    private static final String milesString = "Miles";
    private int mDefaultBedRoomValue = 0;
    private int mDefaultBathRoomValue = 0;
    private int mDefaultSquareFootageValue = 0;
    private int mDefaultSearchRadiusValue = 3;
    private int maxNumberOfZipCode = 6;
    private String[] mBedAndBathRoomsArray;
    private String[] mSquareFootageArray;
    private String[] mSearchRadiusArray;
    /*private SegmentedControlBar foreclosureSegementedControlBar;
    private SegmentedControlBar newBuildsSegementedControlBar;*/
    private TextView featureValue;
    private EditText keyWordEditText;
    SearchBar keyWordSearchBar;
    private LayoutInflater layoutInflater;
    private View dialogView;
    private SegmentedControlBar activeStatusControlBar;
    private String resultFeatureString,selectedFeature;

    // private InputFilter filter = new InputFilter() {
    //
    // public CharSequence filter(CharSequence source, int start, int end,
    // Spanned dest,
    // int dstart, int dend) {
    // for (int i = start; i < end; i++) {
    // if (!Utility.isValidCharacter(source.charAt(i))) {
    // return AppConstants.EMPTY_STRING;
    // }
    // }
    // return null;
    // }
    // };
    @Override
    protected int getUILayout() {
        return R.layout.quick_search;
    }

    @Override
    protected void initializeUI(Bundle savedInstanceState) {
        searchViewManager = new SearchViewManager(this);
        // Initialize Analytics Field
        initializeAnalytics();
        // Initialize Header
        initializeHeader();
        settingsSwitcher = (ViewSwitcher)findViewById(R.id.settings_switcher);
        mBedAndBathRoomsArray = resources.getStringArray(R.array.bed_bath_rooms);
        mSquareFootageArray = resources.getStringArray(R.array.square_footages);
        mSearchRadiusArray = resources.getStringArray(R.array.search_radius);
        // wiring location search views
        wireLocationSearchView();
        // wiring mls search views
        wireMlsSearchView();
        // gps Call
        initializeGps();
        // Add asterisk to mandatory fields
        // HomeSetupUtil.markAsterisk((TextView)
        // findViewById(R.id.search_with));
        /*
         * HomeSetupUtil .markAsterisk((TextView) findViewById(R.id.req_field_legend));
         */
        mlsClearButton = (ImageButton)findViewById(R.id.mls_clear_btn);
        mlsClearButton.setOnClickListener(this);
        // Set default value of preferences to "true"
        PreferenceManager
                .put(QuickSearchActivity.this, Keys.PreferenceKeys.PREF_SHOW_ADDRESS, true);
        PreferenceManager.put(QuickSearchActivity.this, Keys.PreferenceKeys.PREF_SHOW_PRICE, true);
        /*
         * PreferenceManager.put(QuickSearchActivity.this,
         * Keys.PreferenceKeys.PREF_COPY_MYSELF_ON_EMAILS, true);
         */
        activeStatusControlBar = (SegmentedControlBar)findViewById(R.id.active_status_btn);
        activeStatusControlBar.setVisibilityStatus(View.VISIBLE, View.VISIBLE, View.GONE);
        activeStatusControlBar.setTextStatus(getString(R.string.on),
                getString(R.string.off), null);
        activeStatusControlBar.setHeight(AppConstants.INT_THIRTY);
        activeStatusControlBar.setWidth(AppConstants.INT_FIFTY, AppConstants.INT_FIFTY,
                AppConstants.INT_ZERO);
        activeStatusControlBar.setSegmentBtnListener(this);
        activeStatusControlBar.setCheckedStatus(R.id.radio_btn_2);
    }

    private void initializeAnalytics() {
        trackEvent = getString(R.string.Screen_Load);
        pageName = getString(R.string.Get_Started_View);
    }

    private void initializeHeader() {
        header = (PageHeader)findViewById(R.id.header);
        // header.setHeaderTitle(resources.getString(R.string.quick_search));
        header.setHeaderTitle(resources.getString(R.string.get_started));
        findViewById(R.id.mls_layout).setPadding(0, PixelSize.getDimensionInDP(5), 0,
                PixelSize.getDimensionInDP(5));
        SearchBar searchBar = (SearchBar)findViewById(R.id.location_header);
        searchBar.setButtonClickListener(this);
        searchBar.editableActAsButton();
        findViewById(R.id.bar).setBackgroundResource(R.drawable.default_header_bg);
        // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void wireLocationSearchView() {
        findViewById(R.id.price_minmax_layout).setOnClickListener(this);
        findViewById(R.id.home_type_layout).setOnClickListener(this);
        findViewById(R.id.square_footage_layout).setOnClickListener(this);
        findViewById(R.id.search_radius_layout).setOnClickListener(this);
        findViewById(R.id.bathrooms_layout).setOnClickListener(this);
        findViewById(R.id.bedrooms_layout).setOnClickListener(this);
        findViewById(R.id.feature_layout).setOnClickListener(this);
        featureValue = (TextView) findViewById(R.id.features_value_txt);
        searchButton = (Button)findViewById(R.id.search);
        searchButton.setOnClickListener(this);
        setSearchEnabled(true);
        priceRangeValue = (TextView)findViewById(R.id.price_minmax_value_txt);
        priceRangeValue.setText(AppConstants.NO_MIN + " - " + AppConstants.NO_MAX);
        homeTypeValue = (TextView)findViewById(R.id.home_type_value_txt);
        homeTypeValue.setText(AppConstants.ANY);
        mBedRoomTextView = (TextView)findViewById(R.id.bedrooms_value_txt);
        mBathRoomTextView = (TextView)findViewById(R.id.bathrooms_value_txt);
        mSquareFootageTextView = (TextView)findViewById(R.id.square_footage_value_txt);
        mSearchRadiusTextView = (TextView)findViewById(R.id.search_radius_value_txt);
        locationBox = (EditText)findViewById(R.id.location_editbox);
        locationBox.setOnClickListener(this);
        /* SegmentedControlBar for Foreclosure
        foreclosureSegementedControlBar = (SegmentedControlBar)findViewById(R.id.fore_closure_segmented_btn);
        foreclosureSegementedControlBar.setVisibilityStatus(View.VISIBLE, View.VISIBLE, View.GONE);
        foreclosureSegementedControlBar.setTextStatus(getString(R.string.on),
                getString(R.string.off), null);
        foreclosureSegementedControlBar.setHeight(AppConstants.INT_THIRTY);
        foreclosureSegementedControlBar.setWidth(AppConstants.INT_FIFTY, AppConstants.INT_FIFTY,
                AppConstants.INT_ZERO);
        foreclosureSegementedControlBar.setCheckedStatus(R.id.radio_btn_2); // Default selection
        foreclosureSegementedControlBar.setSegmentBtnListener(this);
        /* SegmentedControlBar for New Builds 
        newBuildsSegementedControlBar = (SegmentedControlBar)findViewById(R.id.new_builds_segmented_btn);
        newBuildsSegementedControlBar.setVisibilityStatus(View.VISIBLE, View.VISIBLE, View.GONE);
        newBuildsSegementedControlBar.setTextStatus(getString(R.string.on),
                getString(R.string.off), null);
        newBuildsSegementedControlBar.setHeight(AppConstants.INT_THIRTY);
        newBuildsSegementedControlBar.setWidth(AppConstants.INT_FIFTY, AppConstants.INT_FIFTY,
                AppConstants.INT_ZERO);
        newBuildsSegementedControlBar.setCheckedStatus(R.id.radio_btn_2); // Default selection
        newBuildsSegementedControlBar.setSegmentBtnListener(this); */
        // For Keyword
        keyWordSearchBar = (SearchBar)findViewById(R.id.keyword_header);
        keyWordEditText = (EditText)keyWordSearchBar.findViewById(R.id.location_editbox);
        keyWordSearchBar.setButtonClickListener(this);
        keyWordEditText.setCompoundDrawablePadding(PixelSize.getDimensionInDP(10));
        // set max length to 8000
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(8000);
        keyWordEditText.setFilters(FilterArray);
        keyWordEditText.setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN)
                        && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    listingSummaryRequestData.getFilters().setKeyword(
                            keyWordEditText.getText().toString());
                    //selectedFeature=compareFeatureWithKeywords(keyWordEditText.getText().toString(),resultFeatureString);
                    return true;
                }
                return false;
            }
        });
        SearchWordWatcher searchKeyValidator = new SearchWordWatcher(keyWordEditText);
        keyWordEditText.addTextChangedListener(searchKeyValidator);
        searchKeyValidator.setExtractListener(new SearchKeyExtractListener());
    }

    private void wireMlsSearchView() {
        findViewById(R.id.state_zip_btn).setOnClickListener(this);
        mlsidEditText = (EditText)findViewById(R.id.mlsid_edit);
        // mlsidEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
        mlsidEditText.setOnKeyListener(new OnKeyListener() {

            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN)
                        && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    findMyHome();
                    return true;
                }
                return false;
            }
        });
        mlsidEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (currentView == MLS_VIEW) {
                    if (s.length() > 0) {
                        mlsClearButton.setVisibility(View.VISIBLE);
                    } else {
                        mlsClearButton.setVisibility(View.INVISIBLE);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        });
    }

    private void initializeGps() {
        boolean isGpsEnabled = application.getLocationProvider().isGPSEnabled(this);
        if (!isGpsEnabled) {
            showDialog(AppConstants.DIALOG_GPS_TURNED_OFF);
        }
    }

    public void onItemClick(AdapterView<?> adapter, View parent, int position, long id) {
        inputMethodManager.hideSoftInputFromWindow(mlsidEditText.getWindowToken(), 0);
        String mlsId = adapter.getAdapter().getItem(position).toString();
        mlsidEditText.setText(mlsId);
        mlsidEditText.setSelection(mlsId.length());
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.location_right_button:
                if (currentView != MLS_VIEW) {
                    currentView = MLS_VIEW;
                    settingsSwitcher.setDisplayedChild(currentView);
                    // header.setVisibility(View.GONE);
                    initializeMLSData();
                    mlsidEditText.requestFocus();
                    inputMethodManager.showSoftInput(mlsidEditText, 0);
                    // /setSearchEnabled(false);
                    searchButton.setVisibility(View.INVISIBLE);
                } else {
                    findMyHome();
                }
                break;
            case R.id.price_minmax_layout:
                hideKeyboard();
                showDialog(AppConstants.DIALOG_PRICE_MIN_MAX);
                break;
            case R.id.home_type_layout:
                hideKeyboard();
                showDialog(AppConstants.DIALOG_HOME_TYPE);
                break;
            case R.id.bedrooms_layout:
                hideKeyboard();
                showDialog(AppConstants.DIALOG_BEDROOMS);
                break;
            case R.id.bathrooms_layout:
                hideKeyboard();
                showDialog(AppConstants.DIALOG_BATHROOMS);
                break;
            case R.id.square_footage_layout:
                hideKeyboard();
                showDialog(AppConstants.DIALOG_SQUARE_FOOTAGE);
                break;
            case R.id.search_radius_layout:
                hideKeyboard();
                showDialog(AppConstants.DIALOG_RADIUS);
                break;
            case R.id.state_zip_btn:
            case R.id.location_editbox:
                Intent locationIntent = getIntent();
                locationIntent.setClass(this, SelectLocationActivity.class);
                locationIntent.putExtra(SelectLocationActivity.INTENT_SEARCHADDRESS_VALUE,
                        searchKeyword);
                locationIntent.putExtra(SelectLocationActivity.INTENT_FROM_QS, true);
                startActivityForResult(locationIntent,
                        OpenHouseApplication.REQUEST_CODE_SELECT_LOCATION);
                break;
            case R.id.search:
                if (currentView == MLS_VIEW) {
                    inputMethodManager.hideSoftInputFromWindow(mlsidEditText.getWindowToken(), 0);
                }
                if (TextUtils.isEmpty(searchKeyword)) {
                    showDialog(AppConstants.DIALOG_NO_DATA_IN_SEARCH_BOX);
                } else {
                    findMyHome();
                }
                break;
            case R.id.mls_clear_btn:
                mlsidEditText.setText("");
                if (inputMethodManager != null) {
                    inputMethodManager.toggleSoftInput(0, InputMethodManager.SHOW_IMPLICIT);
                }
                previousSearchedMLSIDs = SearchLocationHistoryDAO.fetchHistory(true);
                historyListAdapter = new HistoryListAdapter(this, previousSearchedMLSIDs);
                locationList.setAdapter(historyListAdapter);
                break;
            case R.id.clear_btn:
                keyWordEditText.setText("");
                listingSummaryRequestData.getFilters().setKeyword(
                        keyWordEditText.getText().toString());
                break;
            case R.id.feature_layout:
            	showDialog(AppConstants.DIALOG_REFINE_SEARCH_FEATURE);
            	break;
            default:
                break;
        }
    }

    private void refreshSettingFields() {
        searchKeyword = "";
        locationBox.setText(searchKeyword);
        priceRangeValue.setText(AppConstants.NO_MIN + " - " + AppConstants.NO_MAX);
        homeTypeValue.setText(AppConstants.ANY);
        mBedRoomTextView.setText(AppConstants.ANY);
        mBathRoomTextView.setText(AppConstants.ANY);
        mSquareFootageTextView.setText(AppConstants.ANY);
        mSearchRadiusTextView.setText(String.valueOf(AreaLocation.DEFAULT_RADIUS));
        homeSetupData.setListingSummaryRequestData(new ListingSummaryRequestData());
    }

    // Initialize MLS ID data
    private void initializeMLSData() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        // start for auto suggest
        locationList = (TransparentListView)findViewById(R.id.location_list);
        locationList.setOnItemClickListener(this);
        previousSearchedMLSIDs = SearchLocationHistoryDAO.fetchHistory(true);
        historyListAdapter = new HistoryListAdapter(this, previousSearchedMLSIDs);
        locationList.setAdapter(historyListAdapter);
        // mlsidEditText.setFilters(new InputFilter[] {
        // filter
        // });
        SearchWordWatcher searchKeyValidator = new SearchWordWatcher(mlsidEditText);
        mlsidEditText.addTextChangedListener(searchKeyValidator);
        searchKeyValidator.setExtractListener(new TextExtract()); // End for
                                                                  // auto
                                                                  // suggest
        refreshMLSSearchFields();
    }

    private void refreshMLSSearchFields() {
        mlsidEditText.setText(AppConstants.EMPTY_STRING);
    }

    // Initiate the search when tap of the Find Home link
    private void findMyHome() {
        if (currentView == STATE_ZIP_VIEW) {
            if (TextUtils.isEmpty(searchKeyword)) {
                showDialog(AppConstants.DIALOG_SEARCH_FIELD_VALIDATOR);
                return;
            }
            if (!TextUtils.isEmpty(searchKeyword)) {
                String[] zipCode = searchKeyword.split(",");
                if (zipCode.length > maxNumberOfZipCode) {
                    showDialog(AppConstants.DIALOG_MAXIMUM_NUMBER_ZIP_CODE_VALIDATOR);
                    return;
                }
            }
            if (!SearchHelper.isFilterAvailable(listingSummaryRequestData.getFilters(), false)) {
                listingSummaryRequestData.setFilters(null);
            }
            // Refresh MLS ID Field data while searching through Personal
            // Settings
            refreshMLSSearchFields();
        } else if (currentView == MLS_VIEW) {
            String mlsId = mlsidEditText.getText().toString();
            if (TextUtils.isEmpty(mlsId)) {
                showDialog(AppConstants.DIALOG_SEARCH_FIELD_ICON_VALIDATOR);
                return;
            }
            listingSummaryRequestData = new ListingSummaryRequestData();
            homeSetupData.setSearchType(HomeSetupData.MLS_SEARCH);
            listingSummaryRequestData.getSearchKey().setListingId(mlsId);
            listingSummaryRequestData.setFilters(null);
            refreshSettingFields();
        }
        // Initiate Search Request
        initiateSearch();
    }

    // Initiate Search Request
    private void initiateSearch() {
        homeSetupData.setPerformServiceRequest(true);
        homeSetupData.setListingSummaryRequestData(listingSummaryRequestData);
        application.setHomeSetupData(homeSetupData);
        // Prepare Search Command as per Home Setup or MLS ID
        searchViewManager.setListingEvent(homeSetupData.getListingSummaryRequestData());
    }

    private void savePersonalSettings() {
        PreferenceManager.put(this, Keys.PreferenceKeys.PERSONAL_SETTINGS, true);
    }

    @Override
    public void onBackPressed() {
        searchButton.setVisibility(View.VISIBLE);
        if (currentView == MLS_VIEW) {
            currentView = STATE_ZIP_VIEW;
            settingsSwitcher.setDisplayedChild(currentView);
            header.setVisibility(View.VISIBLE);
            /*
             * if (TextUtils.isEmpty(searchKeyword)) { setSearchEnabled(true); } /* else {
             * setSearchEnabled(true); }
             */
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (currentView != MLS_VIEW) {
            currentView = MLS_VIEW;
            settingsSwitcher.setDisplayedChild(currentView);
            header.setVisibility(View.VISIBLE);
            initializeMLSData();
            // / setSearchEnabled(false);
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        if (inputMethodManager != null) {
            inputMethodManager.toggleSoftInput(0, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == OpenHouseApplication.REQUEST_CODE_SELECT_LOCATION
                && resultCode == RESULT_OK) {
            searchButton.setVisibility(View.VISIBLE);
            if (currentView == MLS_VIEW) {
                currentView = STATE_ZIP_VIEW;
                settingsSwitcher.setDisplayedChild(currentView);
            }
            if (data != null) {
                if (data.hasExtra(SelectLocationActivity.INTENT_LOCATION_LAT)
                        && data.hasExtra(SelectLocationActivity.INTENT_LOCATION_LONG)) {
                    String latitude = data
                            .getStringExtra(SelectLocationActivity.INTENT_LOCATION_LAT);
                    String longitude = data
                            .getStringExtra(SelectLocationActivity.INTENT_LOCATION_LONG);
                    String place = latitude + ", " + longitude;
                    listingSummaryRequestData.getSearchKey().setArea(
                            new AreaLocation(place, AreaLocation.DEFAULT_RADIUS));
                    searchKeyword = data
                            .getStringExtra(SelectLocationActivity.INTENT_SEARCHADDRESS_VALUE);
                    locationBox.setText(searchKeyword);
                    homeSetupData.setSearchType(HomeSetupData.CURRENT_LOCATION_SEARCH);
                } else {
                    searchKeyword = data
                            .getStringExtra(SelectLocationActivity.INTENT_SEARCHADDRESS_VALUE);
                    locationBox.setText(searchKeyword);
                    listingSummaryRequestData.getSearchKey().setArea(
                            new AreaLocation(searchKeyword, AppConstants.INT_ZERO));
                    homeSetupData.setSearchType(HomeSetupData.LOCATION_SEARCH);
                }
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow(keyWordEditText.getWindowToken(), 0);
                }
                /*
                 * if (TextUtils.isEmpty(searchKeyword)) { setSearchEnabled(false); } else {
                 * setSearchEnabled(true); }
                 */
            }
        }
    }

    @Override
    protected void onPrepareDialog(int id, Dialog dialog) {
        ListView list;
        switch (id) {
            case AppConstants.DIALOG_BEDROOMS:
                list = ((AlertDialog)dialog).getListView();
                list.setSelection(mDefaultBedRoomValue);
                list.setItemChecked(mDefaultBedRoomValue, true);
                break;
            case AppConstants.DIALOG_BATHROOMS:
                list = ((AlertDialog)dialog).getListView();
                list.setSelection(mDefaultBathRoomValue);
                list.setItemChecked(mDefaultBathRoomValue, true);
                break;
            case AppConstants.DIALOG_SQUARE_FOOTAGE:
                list = ((AlertDialog)dialog).getListView();
                list.setSelection(mDefaultSquareFootageValue);
                list.setItemChecked(mDefaultSquareFootageValue, true);
                break;
            case AppConstants.DIALOG_RADIUS:
                list = ((AlertDialog)dialog).getListView();
                list.setSelection(mDefaultSearchRadiusValue);
                list.setItemChecked(mDefaultSearchRadiusValue, true);
                break;
            default:
                break;
        }
        super.onPrepareDialog(id, dialog);
    }

    public Dialog onCreateDialog(int id) {
        layoutInflater = LayoutInflater.from(this);
        dialogView = layoutInflater.inflate(R.layout.myteam_selectcontacts_dialog, null);
        dialogIcon = (ImageView)dialogView.findViewById(R.id.dialog_image);
        dialogTxtMessage = (TextView)dialogView.findViewById(R.id.dialog_text);
        if (isFinishing()) {
            return null;
        }
        SingleChoiceDialogBuilder singleChoiceDialogBuilder = null;
        switch (id) {
            case AppConstants.DIALOG_GPS_TURNED_OFF:
                AlertDialog dialogGpsTurnedOff = new AlertDialog.Builder(this)
                        .setMessage(R.string.gps_turned_off)
                        .setPositiveButton(R.string.settings,
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        final Intent intent = new Intent(
                                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                        startActivity(intent);
                                    }
                                })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }).create();
                dialogGpsTurnedOff.setCancelable(false);
                return dialogGpsTurnedOff;
            case AppConstants.DIALOG_HOME_TYPE:
                MultiChoiceDialog choiceDialog = new MultiChoiceDialog(this);
                choiceDialog.setCancelable(false);
                choiceDialog.setTitle(R.string.home_type);
                choiceDialog.setKeyList(HomeSetupUtil.getKeyList());
                choiceDialog.setValueList(HomeSetupUtil.getValueList());
                choiceDialog.setDefaultIndex(0);
                choiceDialog.isAlterWithDefault(true);
                choiceDialog.setOnUpdateSelection(homeTypeValue.getText().toString());
                choiceDialog.setMultiChoiceListener(new MultiChoiceListner() {

                    @Override
                    public void onMultiChoice(String keys, String values) {
                        if (TextUtils.isEmpty(values) || values.equals(AppConstants.ANY)) {
                            values = AppConstants.ANY;
                            listingSummaryRequestData.getFilters().setPropertyTypes(null);
                            homeSetupData.setPropertyTypes(null);
                        } else {
                            homeSetupData.setPropertyTypes(values);
                            String[] keyStringArray = keys.split(",");
                            listingSummaryRequestData.getFilters().setPropertyTypes(
                                    Arrays.asList(keyStringArray));
                            homeSetupData.setPropertyTypes(values);
                        }
                        homeTypeValue.setText(values);
                    }
                });
                return choiceDialog;
                
            case AppConstants.DIALOG_REFINE_SEARCH_FEATURE:
                MultiChoiceDialog featureChoiceDialog = new MultiChoiceDialog(this);
                featureChoiceDialog.setCanceledOnTouchOutside(false);
                featureChoiceDialog.setTitle(R.string.features);
                featureChoiceDialog.setValueList(HomeSetupUtil.getFeaturesValueList());
                featureChoiceDialog.setDefaultIndex(0);
                featureChoiceDialog.isAlterWithDefault(true);
                featureChoiceDialog.setUpdateView(true);
                featureChoiceDialog.setOnUpdateSelection(featureValue.getText().toString());
                featureChoiceDialog.setMultiChoiceListener(new MultiChoiceListner() {

                    @Override
                    public void onMultiChoice(String keys, String values) {
                        if (TextUtils.isEmpty(values) || values.equals(AppConstants.ANY)) {
                            values = AppConstants.ANY;
                            featureValue.setText(values);
                            listingSummaryRequestData.getFilters().setFeatures(null);
                            homeSetupData.setFeatures(null);
                        } else {
                            listingSummaryRequestData.getFilters().setFeatures(
                                    HomeSetupUtil.getFeaturesKeys(values));
                            featureValue.setText(values);
                            homeSetupData.setFeatures(values);
                        }
                    }
                });
                return featureChoiceDialog;
            case AppConstants.DIALOG_PRICE_MIN_MAX:
                PriceRangePickerDialog priceRangeDialog = new PriceRangePickerDialog(this);
                priceRangeDialog.setCancelable(false);
                priceRangeDialog.setTitle(R.string.price_min_max);
                List<String> list = HomeSetupUtil.getPriceRange();
                List<String> minPriceList = new ArrayList<String>();
                minPriceList.add(AppConstants.NO_MIN);
                minPriceList.addAll(list);
                List<String> maxPriceList = new ArrayList<String>();
                maxPriceList.add(AppConstants.NO_MAX);
                maxPriceList.addAll(list);
                priceRangeDialog.setMaxPriceList(maxPriceList);
                priceRangeDialog.setMinPriceList(minPriceList);
                priceRangeDialog.setOnPriceSetListener(new PriceSet());
                priceRangeDialog.setValidationCallback(new ValidationCallback() {

                    @Override
                    public void onValidationFailed() {
                        showDialog(AppConstants.DIALOG_PRICE_RANGE_SELECTED_VALIDATOR);
                    }
                });
                return priceRangeDialog;
            case AppConstants.DIALOG_BEDROOMS:
                singleChoiceDialogBuilder = new SingleChoiceDialogBuilder(this);
                singleChoiceDialogBuilder.setDefaultChecked(mDefaultBedRoomValue);
                singleChoiceDialogBuilder.setSingleChoiceItems(mBedAndBathRoomsArray);
                singleChoiceDialogBuilder.setTitle(R.string.bedrooms);
                singleChoiceDialogBuilder.setDialogActionListner(new DialogListenerBedrooms());
                AlertDialog dialogBedRooms = singleChoiceDialogBuilder.create();
                dialogBedRooms.setCancelable(false);
                return dialogBedRooms;
            case AppConstants.DIALOG_BATHROOMS:
                singleChoiceDialogBuilder = new SingleChoiceDialogBuilder(this);
                singleChoiceDialogBuilder.setDefaultChecked(mDefaultBathRoomValue);
                singleChoiceDialogBuilder.setSingleChoiceItems(mBedAndBathRoomsArray);
                singleChoiceDialogBuilder.setTitle(R.string.bathrooms);
                singleChoiceDialogBuilder.setDialogActionListner(new DialogListenerBathrooms());
                AlertDialog dialogBathRooms = singleChoiceDialogBuilder.create();
                dialogBathRooms.setCancelable(false);
                return dialogBathRooms;
            case AppConstants.DIALOG_SQUARE_FOOTAGE:
                singleChoiceDialogBuilder = new SingleChoiceDialogBuilder(this);
                singleChoiceDialogBuilder.setDefaultChecked(mDefaultSquareFootageValue);
                singleChoiceDialogBuilder.setSingleChoiceItems(mSquareFootageArray);
                singleChoiceDialogBuilder.setTitle(R.string.square_footage);
                singleChoiceDialogBuilder.setDialogActionListner(new DialogActionSqFootage());
                AlertDialog dialogSquareFootage = singleChoiceDialogBuilder.create();
                dialogSquareFootage.setCancelable(false);
                return dialogSquareFootage;
            case AppConstants.DIALOG_RADIUS:
                singleChoiceDialogBuilder = new SingleChoiceDialogBuilder(this);
                singleChoiceDialogBuilder.setDefaultChecked(mDefaultSearchRadiusValue);
                singleChoiceDialogBuilder.setSingleChoiceItems(mSearchRadiusArray);
                singleChoiceDialogBuilder.setTitle(R.string.search_radius);
                singleChoiceDialogBuilder.setDialogActionListner(new DialogActionRadius());
                AlertDialog dialogRadius = singleChoiceDialogBuilder.create();
                dialogRadius.setCancelable(false);
                return dialogRadius;
            case AppConstants.DIALOG_SEARCH_FIELD_ICON_VALIDATOR:
                /*LayoutInflater mlsLayout = LayoutInflater.from(this);
                View mlsInfoLayout = mlsLayout.inflate(R.layout.myteam_selectcontacts_dialog, null);
                mlsInfoLayout.setPadding(25, 0, 25, 0);
                ImageView mIcon = (ImageView)mlsInfoLayout.findViewById(R.id.dialog_image);
                TextView mlsMessage = (TextView)mlsInfoLayout.findViewById(R.id.dialog_text);
                mIcon.setImageResource(R.drawable.info_transparent_i);
                mlsMessage.setText(getString(R.string.mls_id_field_validator));
                mlsMessage.setTextSize(18);*/
                dialogIcon.setImageResource(R.drawable.info_transparent_i);
                dialogTxtMessage.setText(getString(R.string.mls_id_field_validator));
                dialogTxtMessage.setTextSize(18);
                AlertDialog dialogSearchFieldIconValidator = new AlertDialog.Builder(this).setView(dialogView)
                        .setIcon(0)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }).create();
                dialogSearchFieldIconValidator.setCancelable(false);
                return dialogSearchFieldIconValidator;
            case AppConstants.DIALOG_SEARCH_FIELD_VALIDATOR:
                /*LayoutInflater factory = LayoutInflater.from(this);
                View layout = factory.inflate(R.layout.myteam_selectcontacts_dialog, null);
                layout.setPadding(25, 0, 25, 0);
                ImageView icon = (ImageView)layout.findViewById(R.id.dialog_image);
                TextView message = (TextView)layout.findViewById(R.id.dialog_text);
                icon.setImageResource(R.drawable.info_transparent_i);
                message.setText(getString(R.string.search_field_validator));
                message.setTextSize(18);*/
                dialogIcon.setImageResource(R.drawable.info_transparent_i);
                dialogTxtMessage.setText(getString(R.string.search_field_validator));
                dialogTxtMessage.setTextSize(18);
                AlertDialog searchFieldValidator = new AlertDialog.Builder(this).setView(dialogView)
                        .setIcon(0)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }).create();
                searchFieldValidator.setCancelable(false);
                return searchFieldValidator;
            case AppConstants.DIALOG_MAXIMUM_NUMBER_ZIP_CODE_VALIDATOR:
                AlertDialog zipCodeValidator = new AlertDialog.Builder(this)
                        .setMessage(R.string.max_search_field_validation).setIcon(0)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }).create();
                zipCodeValidator.setCancelable(false);
                return zipCodeValidator;
            case AppConstants.DIALOG_PRICE_RANGE_SELECTED_VALIDATOR:
                /*LayoutInflater priceRangeFactory = LayoutInflater.from(this);
                View priceRangeLayout = priceRangeFactory.inflate(R.layout.myteam_selectcontacts_dialog, null);
                priceRangeLayout.setPadding(25, 0, 25, 0);
                ImageView priceRangeIcon = (ImageView)priceRangeLayout.findViewById(R.id.dialog_image);
                TextView priceRangeMessage = (TextView)priceRangeLayout.findViewById(R.id.dialog_text);
                priceRangeIcon.setImageResource(R.drawable.info_transparent_i);
                priceRangeMessage.setText(getString(R.string.price_range_selected_validator));
                priceRangeMessage.setTextSize(18);*/
                dialogIcon.setImageResource(R.drawable.info_transparent_i);
                dialogTxtMessage.setText(getString(R.string.price_range_selected_validator));
                dialogTxtMessage.setTextSize(18);
                AlertDialog priceRangeValidator = new AlertDialog.Builder(this).setView(dialogView)
                        .setIcon(0)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                showDialog(AppConstants.DIALOG_PRICE_MIN_MAX);
                            }
                        }).create();
                priceRangeValidator.setCancelable(false);
                return priceRangeValidator;
                /*AlertDialog priceRangeValidator = new AlertDialog.Builder(this)
                        .setMessage(R.string.price_range_selected_validator).setIcon(0)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                showDialog(AppConstants.DIALOG_PRICE_MIN_MAX);
                            }
                        }).create();
                priceRangeValidator.setCanceledOnTouchOutside(false);
                return priceRangeValidator;*/
            case AppConstants.DIALOG_INVALID_REQUEST_FORMAT:
                AlertDialog invalidRequestDialog = new AlertDialog.Builder(this)
                        .setMessage(R.string.invalid_request).setIcon(0)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }).create();
                invalidRequestDialog.setCancelable(false);
                return invalidRequestDialog;
            case AppConstants.DIALOG_NO_RESULT_FOUND:
                /*LayoutInflater noResultFoundFactory = LayoutInflater.from(this);
                View noResultFoundLayout = noResultFoundFactory.inflate(R.layout.myteam_selectcontacts_dialog, null);
                noResultFoundLayout.setPadding(25, 0, 25, 0);
                ImageView noResultFoundIcon = (ImageView)noResultFoundLayout.findViewById(R.id.dialog_image);
                TextView noResultFoundMessage = (TextView)noResultFoundLayout.findViewById(R.id.dialog_text);
                noResultFoundIcon.setImageResource(R.drawable.info_transparent_i);
                noResultFoundMessage.setText(getString(R.string.no_results));
                noResultFoundMessage.setTextSize(18);*/
                dialogIcon.setImageResource(R.drawable.info_transparent_i);
                dialogTxtMessage.setText(getString(R.string.no_results));
                dialogTxtMessage.setTextSize(18);
                AlertDialog noResultFoundDialog = new AlertDialog.Builder(this).setView(dialogView)
                        .setIcon(0)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }).create();
                noResultFoundDialog.setCancelable(false);
                return noResultFoundDialog;
            case AppConstants.DIALOG_NO_MLS_RESULT_FOUND:
                /*LayoutInflater noMlsResultFoundFactory = LayoutInflater.from(this);
                View noMlsResultFoundLayout = noMlsResultFoundFactory.inflate(R.layout.myteam_selectcontacts_dialog, null);
                noMlsResultFoundLayout.setPadding(25, 0, 25, 0);
                ImageView noMlsResultFoundIcon = (ImageView)noMlsResultFoundLayout.findViewById(R.id.dialog_image);
                TextView noMlsResultFoundMessage = (TextView)noMlsResultFoundLayout.findViewById(R.id.dialog_text);
                noMlsResultFoundIcon.setImageResource(R.drawable.info_transparent_i);
                noMlsResultFoundMessage.setText(getString(R.string.no_mlsid_results));
                noMlsResultFoundMessage.setTextSize(18);*/
                dialogIcon.setImageResource(R.drawable.info_transparent_i);
                dialogTxtMessage.setText(getString(R.string.no_mlsid_results));
                dialogTxtMessage.setTextSize(18);
                AlertDialog noMlsResultFoundDialog = new AlertDialog.Builder(this).setView(dialogView)
                        .setIcon(0)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }).create();
                noMlsResultFoundDialog.setCancelable(false);
                return noMlsResultFoundDialog;
            case AppConstants.DIALOG_NETWORK_ERROR:
                OHErrorDialog dialogNetworkError = new OHErrorDialog(this, getResources()
                        .getString(R.string.alert_dialog_title), getResources().getString(
                        R.string.no_network_message), false);
                dialogNetworkError.setCancelable(false);
                return dialogNetworkError;
            case AppConstants.DIALOG_SERVER_ERROR:
                OHErrorDialog dialogServerError = new OHErrorDialog(this, getResources().getString(
                        R.string.error_dialog_title), getResources().getString(
                        R.string.server_unresponsive_letstart), false);
                dialogServerError.setCancelable(false);
                return dialogServerError;
            
            case AppConstants.DIALOG_NO_DATA_IN_SEARCH_BOX:
                /*LayoutInflater dialogNoDataLayout = LayoutInflater.from(this);
                View view = dialogNoDataLayout.inflate(R.layout.myteam_selectcontacts_dialog, null);
                ImageView image = (ImageView)view.findViewById(R.id.dialog_image);
                image.setImageResource(R.drawable.ic_dialog_nocontact_selected);
                TextView dialogNoDataText = (TextView)view.findViewById(R.id.dialog_text);
                dialogNoDataText.setText(R.string.no_values_entered);
                dialogNoDataText.setTextSize(18);*/
                dialogIcon.setImageResource(R.drawable.info_transparent_i);
                dialogTxtMessage.setText(getString(R.string.search_field_validator));
                dialogTxtMessage.setTextSize(18);
                AlertDialog dialogNoDataInSearchBox = new AlertDialog.Builder(this).setView(dialogView)
                        .setIcon(0)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }).create();
                dialogNoDataInSearchBox.setCancelable(false);
                return dialogNoDataInSearchBox;
            /*case AppConstants.DIALOG_REFINE_SEARCH_FEATURE:
            	List<String> keyList = new ArrayList<String>();
                for (int i = 0; i < getResources().getStringArray(R.array.features).length; i++) {
                    keyList.add(String.valueOf(i));
                }
                MultiChoiceDialog featureChoiceDialog = new MultiChoiceDialog(this);
                featureChoiceDialog.setCancelable(false);
                featureChoiceDialog.setTitle(getResources().getString(R.string.features_txt));
                featureChoiceDialog.setKeyList(keyList);
                featureChoiceDialog.setValueList(Arrays.asList(getResources().getStringArray(R.array.features)));
                featureChoiceDialog.setDefaultIndex(0);
                featureChoiceDialog.isAlterWithDefault(true);
                featureChoiceDialog.setUpdateView(true);
                featureChoiceDialog.setOnUpdateSelection(resultFeatureString);
                featureChoiceDialog.setMultiChoiceListener(new MultiChoiceListner() {
                    @Override
                    public void onMultiChoice(String keys, String values) {
                    	resultFeatureString=values;
                    	featureValue.setText(resultFeatureString);
                    }
                });
                return featureChoiceDialog;*/
        }
        return super.onCreateDialog(id);
    }
    
    private String compareFeatureWithKeywords(String keyword,String featurevalue) {
    	List<String> featuredata = Arrays.asList(featurevalue.split(","));
    	List<String> keydata = Arrays.asList(keyword.split(","));
    	String Result="",requiredKeyword="",requiredFeature="";
    	for(int i=0;i<keydata.size();i++){
    		for(int j=0;j<featuredata.size();j++){
    			requiredKeyword=keydata.get(i).trim();
    			requiredFeature=featuredata.get(j).trim();
    			if(requiredKeyword.equalsIgnoreCase(requiredFeature)){
    				if(Result.equals(""))
    					Result=keydata.get(i);
    				else
    					Result=Result+","+keydata.get(i);
    			}
    		}
    	}
		return Result;
	}

    @Override
    public boolean handleResponseMessage(com.chase.core.model.Response<?> response) {
        return true;
    }

    public void setData(Response<?> response) {
        if (response.getAction() == ServiceActions.MLS_LISTING_SUMMARY) {
            if (response.getMessageType() == ResponseType.RESPONSE_SUCCESS) {
            	/**here we are making this one as true so that 
            	 * on fersh build of 2.5 it should not hit the
            	 * services by making if (!SearchHelper.isProcessRequest(homeSetupData) && PreferenceManager.getBoolean(this, Keys.PreferenceKeys.PREF_NEED_TO_REFRESH_ON_UPGRADE)) {
            	 * as false 
            	 * */
            	PreferenceManager.put(this, Keys.PreferenceKeys.PREF_NEED_TO_REFRESH_ON_UPGRADE,true);
                onListingLoadFinish();
            } else {
                onListingLoadError(response);
            }
        }
    }

    private void onListingLoadFinish() {
        // Saved the First Run Flow
        dismissDialog();
        savePersonalSettings();
        Intent searchIntent = new Intent();
        // TODO: Remove tab
        // searchIntent.setClass(this, NavigatorActivity.class);
        searchIntent.setClass(this, SearchActivity.class);
        startActivity(searchIntent);
        finish();
    }

    private void onListingLoadError(Response<?> response) {
        dismissDialog();
        if (response.getMessageType() == ResponseType.RESPONSE_CONNECTION_ERROR) {
            showDialog(AppConstants.DIALOG_NETWORK_ERROR);
        } else if (response.getMessageType() == ResponseType.RESPONSE_FAILURE) {
            showDialog(AppConstants.DIALOG_SERVER_ERROR);
        } else if (response.getMessageType() == ResponseType.SC_SERVICE_UNAVAILABLE) {
            showDialog(AppConstants.DIALOG_SERVER_ERROR);
        } else if (response.getMessageType() == ResponseType.RESPONSE_INVALID_REQUEST_FORMAT) {
            showDialog(AppConstants.DIALOG_INVALID_REQUEST_FORMAT);
        } else if (response.getMessageType() == ResponseType.RESPONSE_ZERO_RECORDCOUNT) {
            if (currentView == MLS_VIEW) {
                showDialog(AppConstants.DIALOG_NO_MLS_RESULT_FOUND);
            } else {
                showDialog(AppConstants.DIALOG_NO_RESULT_FOUND);
            }
        } else {
            showDialog(AppConstants.DIALOG_NO_RESULT_FOUND);
        }
    }

    private void setSearchEnabled(boolean isEnabled) {
        searchButton.setEnabled(isEnabled);
    }

    /*
     * @Override public boolean onKeyDown(int keyCode, KeyEvent event) {
     * setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); return
     * super.onKeyDown(keyCode, event); }
     */
    private class TextExtract implements TextExtractListener {

        @Override
        public void onTextExtract(String word) {
            if (currentView == MLS_VIEW) {
                searchButton.setVisibility(View.INVISIBLE);
                if (TextUtils.isEmpty(word)) {
                    historyListAdapter.setItems(previousSearchedMLSIDs);
                    // / setSearchEnabled(false);
                } else {
                    List<String> tempList = new ArrayList<String>();
                    for (int i = 0; i < previousSearchedMLSIDs.size(); i++) {
                        if (previousSearchedMLSIDs.get(i).toLowerCase().startsWith(word)) {
                            tempList.add(previousSearchedMLSIDs.get(i));
                        }
                    }
                    historyListAdapter.setItems(tempList);
                    // / setSearchEnabled(true);
                }
            }
        }
    }

    private class PriceSet implements OnPriceSetListener {

        @Override
        public void onPriceSet(String minPrice, String maxPrice) {
            if (!minPrice.equals(AppConstants.NO_MIN)) {
                minPrice = AppConstants.DOLLER + minPrice;
                listingSummaryRequestData.getFilters().setMinPrice(
                        Utility.removeDollarFormat(minPrice));
            } else {
                listingSummaryRequestData.getFilters().setMinPrice(null);
            }
            if (!maxPrice.equals(AppConstants.NO_MAX)) {
                maxPrice = AppConstants.DOLLER + maxPrice;
                listingSummaryRequestData.getFilters().setMaxPrice(
                        Utility.removeDollarFormat(maxPrice));
            } else {
                listingSummaryRequestData.getFilters().setMaxPrice(null);
            }
            priceRangeValue.setText(String.format("%s - %s", minPrice, maxPrice));
        }
    }

    private class DialogListenerBedrooms implements DialogActionListner {

        @Override
        public void onPositiveButtonClick(int selectedItemPosition, String selectedItemValue) {
            if (selectedItemPosition >= AppConstants.INT_ZERO) {
                mDefaultBedRoomValue = selectedItemPosition;
            }
            mSelectedBedRoomValue = mBedAndBathRoomsArray[mDefaultBedRoomValue];
            mBedRoomTextView.setText(mSelectedBedRoomValue);
            String bedRoomString = mSelectedBedRoomValue;
            if (!bedRoomString.contains(AppConstants.ANY)) {
                bedRoomString = bedRoomString.substring(0, bedRoomString.length() - 1);
                /*
                 * homeSetupData.getListingSummaryRequestData().getFilters()
                 * .setMinBedrooms(bedRoomString);
                 */
                // mBedRoomTextView.setText(bedRoomString);
                listingSummaryRequestData.getFilters().setMinBedrooms(bedRoomString);
            } else {
                // homeSetupData.getListingSummaryRequestData().getFilters().setMinBedrooms(null);
                // mBedRoomTextView.setText(bedRoomString);
                listingSummaryRequestData.getFilters().setMinBedrooms(null);
            }
        }

        @Override
        public void onNegativeButtonClick() {}
    }

    private class DialogListenerBathrooms implements DialogActionListner {

        @Override
        public void onPositiveButtonClick(int selectedItemPosition, String selectedItemValue) {
            if (selectedItemPosition >= AppConstants.INT_ZERO) {
                mDefaultBathRoomValue = selectedItemPosition;
            }
            mSelectedBathRoomValue = mBedAndBathRoomsArray[mDefaultBathRoomValue];
            mBathRoomTextView.setText(mSelectedBathRoomValue);
            String bathRoomString = mSelectedBathRoomValue;
            if (!bathRoomString.contains(AppConstants.ANY)) {
                bathRoomString = bathRoomString.substring(0, bathRoomString.length() - 1);
                /*
                 * homeSetupData.getListingSummaryRequestData().getFilters()
                 * .setMinBathrooms(bathRoomString); mBathRoomTextView.setText(bathRoomString);
                 */
                listingSummaryRequestData.getFilters().setMinBathrooms(bathRoomString);
            } else {
                // homeSetupData.getListingSummaryRequestData().getFilters().setMinBathrooms(null);
                // mBathRoomTextView.setText(bathRoomString);
                listingSummaryRequestData.getFilters().setMinBathrooms(null);
            }
        }

        @Override
        public void onNegativeButtonClick() {}
    }

    private class DialogActionSqFootage implements DialogActionListner {

        @Override
        public void onPositiveButtonClick(int selectedItemPosition, String selectedItemValue) {
            if (selectedItemPosition >= AppConstants.INT_ZERO) {
                mDefaultSquareFootageValue = selectedItemPosition;
            }
            mSelectedSquareFootageValue = mSquareFootageArray[mDefaultSquareFootageValue];
            mSquareFootageTextView.setText(mSelectedSquareFootageValue);
            String squareFootageString = mSelectedSquareFootageValue;
            if (!squareFootageString.contains(AppConstants.ANY)) {
                squareFootageString = squareFootageString.substring(0,
                        squareFootageString.length() - 1);
                /*
                 * homeSetupData.getListingSummaryRequestData().getFilters()
                 * .setMinLivingArea(squareFootageString);
                 * mSquareFootageTextView.setText(squareFootageString);
                 */
                listingSummaryRequestData.getFilters().setMinLivingArea(squareFootageString);
            } else {
                // homeSetupData.getListingSummaryRequestData().getFilters().setMinLivingArea(null);
                // mSquareFootageTextView.setText(squareFootageString);
                listingSummaryRequestData.getFilters().setMinLivingArea(null);
            }
        }

        @Override
        public void onNegativeButtonClick() {}
    }

    private class DialogActionRadius implements DialogActionListner {

        @Override
        public void onPositiveButtonClick(int selectedItemPosition, String selectedItemValue) {
            if (selectedItemPosition >= AppConstants.INT_ZERO) {
                mDefaultSearchRadiusValue = selectedItemPosition;
            }
            mSelectedSearchRadiusValue = mSearchRadiusArray[mDefaultSearchRadiusValue];
            mSearchRadiusTextView.setText(mSelectedSearchRadiusValue);
            String tempValues = mSelectedSearchRadiusValue;
            if (tempValues.contains(milesString)) {
                tempValues = tempValues.substring(0, tempValues.length() - 5).trim();
            } else {
                tempValues = tempValues.substring(0, 1).trim();
            }
            /*
             * homeSetupData.getListingSummaryRequestData().getSearchKey().getArea()
             * .setRadius(Integer.parseInt(tempValues));
             */
            // mSearchRadiusTextView.setText(tempValues);
            listingSummaryRequestData.getSearchKey().getArea()
                    .setRadius(Integer.parseInt(tempValues));
        }

        @Override
        public void onNegativeButtonClick() {}
    }

    /*@Override
    public String onSegmentButtonClicked(View view, String buttonValue) {
        switch (view.getId()) {
            case R.id.radio_btn_1:
                if (((View)view.getParent()).getId() == foreclosureSegementedControlBar.getId()) {
                    listingSummaryRequestData.getFilters().setForeclosureOnly(true);
                } else {
                    listingSummaryRequestData.getFilters().setNewConstructionOnly(true);
                }
                return null;
            case R.id.radio_btn_2:
                if (((View)view.getParent()).getId() == foreclosureSegementedControlBar.getId()) {
                    listingSummaryRequestData.getFilters().setForeclosureOnly(false);
                } else {
                    listingSummaryRequestData.getFilters().setNewConstructionOnly(false);
                }
                return null;
            default:
                return null;
        }
    }*/

    private void hideKeyboard() {
        inputMethodManager.hideSoftInputFromWindow(keyWordEditText.getWindowToken(), 0);
    }

    private class SearchKeyExtractListener implements TextExtractListener {

        @Override
        public void onTextExtract(String word) {
            if (TextUtils.isEmpty(word)) {
                keyWordSearchBar.setClearBtnVisibility(View.GONE);
            } else {
                keyWordSearchBar.setClearBtnVisibility(View.VISIBLE);
            }
        }
    }

	@Override
	public String onSegmentButtonClicked(View view, String buttonValue) {
		switch(view.getId()){
		case R.id.radio_btn_1:
			listingSummaryRequestData.getFilters().setListingStatus("Active");
			homeSetupData.setListingStatus("Active");
			break;
		case R.id.radio_btn_2:
			listingSummaryRequestData.getFilters().setListingStatus(null);
			homeSetupData.setListingStatus(null);
			break;
		}
		return null;
	}
}