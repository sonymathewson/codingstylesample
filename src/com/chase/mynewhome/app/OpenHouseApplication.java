package com.chase.mynewhome.app;

import java.util.List;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import com.chase.core.BaseCommand;
import com.chase.core.db.DatabaseProvider;
import com.chase.core.util.GenericLooper;
import com.chase.core.util.LocationProvider;
import com.chase.core.util.OpenHouseLog;
import com.chase.core.util.PreferenceManager;
import com.chase.mynewhome.db.NotificationDAO;
import com.chase.mynewhome.model.HomeSetupData;
import com.chase.mynewhome.model.MortgageBankerResponse;
import com.chase.mynewhome.model.SortingFields;
import com.chase.mynewhome.model.listing.Research;
import com.chase.mynewhome.model.listing.SummaryListings;
import com.chase.mynewhome.util.AnalyticsUtility;
import com.chase.mynewhome.util.Keys;

//Application class

public class OpenHouseApplication extends Application {

	// Request Code
	public static final int REQUEST_CODE_SELECT_LOCATION = 10000;
	public static final int REQUEST_CODE_REFINE_SETUP = 10001;
	public static final int REQUEST_CODE_LISTING_DETAILS = 10002;
	public static final int REQUEST_CODE_REFINE_SELECT_LOCATION = 10003;
	public static final int REQUEST_CODE_SELECT_LOCATION_FROM_MLS = 10004;
	public static final int REQUEST_CODE_JOURNAL = 10005;
	public static final int REQUEST_CODE_MYHOME = 10006;
	public static final int REQUEST_CODE_RECENT_VIEWED = 10007;
	public static final int REQUEST_CODE_AFFORDABILITY_CALCULATOR_ACTIVITY = 10008;
	public static final int REQUEST_CODE_CASHBACK_CALCULATOR_ACTIVITY = 10009;
	public static final int REQUEST_CODE_PAYMENT_ESTIMATOR_CALCULATOR_ACTIVITY = 10010;
	public static final int REQUEST_CODE_MORTGAGE_BANKER_SEARCH = 10011;
	public static final int REQUEST_CODE_MORTGAGE_BANKER_SUMMARY = 10014;
	public static final int REQUEST_CODE_ADD_NEW_CONTACT = 10012;
	public static final int REQUEST_CODE_NEW_REQUEST_CONSULTATION = 10013;
	public static final int REQUEST_CODE_VIEW_CONTACT_DETAILS = 10014;
	public static final int REQUEST_CODE_SEND_EMAIL = 10015;
	public static final int REQUEST_CODE_ADD_NEW_MY_INFO_CONTACT = 10016;
	public static final int REQUEST_CODE_ADD_ADDRESSS_MY_INFO_CONTACT = 10017;
	public static final int REQUEST_CODE_APP_TUTORIAL_BUTTON = 10018;
	public static final int REQUEST_CODE_NEIGHBORHOOD_INFO = 10019;
	public static final int REQUEST_CODE_PRODUCT_LIST = 10020;
	public static final int REQUEST_CODE_FOR_CHECKIN_FROM_ACTIVITY = 10021;
	private static OpenHouseApplication theInstance;
	private LocationProvider locationProvider;
	private DatabaseProvider databaseProvider;
	private HomeSetupData homeSetupData;
	private float density;
	private boolean isFavoriteUpdated;
	private SortingFields sortingFields;
	private SortingFields favoriteSortingFields;
	private List<SummaryListings> favoritesSummaryList;
	private boolean quickLinksMenuOpen = false;
	private int notificationCount = 0;
	private long myTeamGroupId = 100001;
	private String myTeamGroupName = "MyTeam";
	private List<MortgageBankerResponse> bankerListResponse;
	private boolean useDevelopmentEnvironment = false;
	/** artf708202 - when max price (5000000+) is chosen set this as true **/
	private boolean filterMaxPriceChoosen = false;
	/** Added for scrolling issue to maintain the listitem position **/
	private int firstVisibleItem = 0;
	private int listingOffset = 0;
	private int favoriteItemClicked = 0;
	/** Added for Chase different service environment picker */
	private int chaseEnvironmentPickerPosition = 3;
	private int chaseAnalyticEnvironmentPickerPosition = 4;
	private String plottedMapCordinates;
	/**Added for Upgradecheck  **/
	private boolean upgradeCheck = false;
	private boolean refreashNotificationsMenu=false;

	public static OpenHouseApplication getApp() {
		return theInstance;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		this.appHttpQueue.start();
		theInstance = this;
		databaseProvider = new DatabaseProvider(this);
		density = getResources().getDisplayMetrics().density;
		// initialize Analytics on Launch of App
		boolean analyticsDebugLogging = isDevelopmentMode();
		AnalyticsUtility.initializeAnalytics(this, analyticsDebugLogging);
		// createMyTeamGroupInContacts();
	}


	@Override
	public void onTerminate() {
		super.onTerminate();
		AnalyticsUtility.onPageLoad("Android Mortgage App", null, null, null,
				favoritesSummaryList.size());
		this.appHttpQueue.requestStop();
	}

	public float getDensity() {
		return density;
	}

	//Threadpool
	
	private GenericLooper appHttpQueue = new GenericLooper();

	public synchronized void processRequest(BaseCommand<?, ?> command) {
		if (command.isHandleUsingsThread()) {
			if (command.putAtFrontOfQueue) {
				appHttpQueue.enqueCommandAtFrontOfQueue(command);
			} else {
				if (command.delayInMillis > 0) {
					appHttpQueue.enqueCommandWithDelay(command,
							command.delayInMillis);
				} else {
					appHttpQueue.enqueueCommand(command);
				}
			}
		} else {
			command.execute();
		}
	}

	public boolean isConnected(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = null;
		if (connectivityManager != null) {
			networkInfo = connectivityManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		}
		return networkInfo == null ? false : networkInfo.isConnected();
	}

	private SharedPreferences sharedPreferences = null;
	private SharedPreferences sharedPushMessagePreferences,
			sharedIpPreferences = null;

	public SharedPreferences getSharePreferences(String key) {
		if ("user-credentials".equalsIgnoreCase(key)) {
			if (sharedPreferences == null) {
				sharedPreferences = getSharedPreferences(key, 0);
			}
			return sharedPreferences;
		} else if ("track_history".equalsIgnoreCase(key)) {
			return getSharedPreferences(key, 0);
		} else if ("ip".equalsIgnoreCase(key)) {
			if (sharedIpPreferences == null) {
				sharedIpPreferences = getSharedPreferences(key, 0);
			}
			return sharedIpPreferences;
		} else {
			if (sharedPushMessagePreferences == null) {
				sharedPushMessagePreferences = getSharedPreferences(key, 0);
			}
			return sharedPushMessagePreferences;
		}
	}

	public HomeSetupData getHomeSetupData() {
		return homeSetupData;
	}

	public void setHomeSetupData(HomeSetupData homeSetupData) {
		this.homeSetupData = homeSetupData;
	}

	public DatabaseProvider getDatabaseProvider() {
		return databaseProvider;
	}

	public LocationProvider getLocationProvider() {
		if (locationProvider == null) {
			locationProvider = new LocationProvider(this);
		}
		return locationProvider;
	}

	public void openDatabaseIfClosed() {
		if (!databaseProvider.isOpened()) {
			databaseProvider.openDatabase();
		}
	}

	public void closeDatabaseIfOpened() {
		databaseProvider.closeDatabase();
	}

	public boolean isFavoriteUpdated() {
		return isFavoriteUpdated;
	}

	public void setFavoriteUpdated(boolean isFavoriteUpdated) {
		this.isFavoriteUpdated = isFavoriteUpdated;
	}

	/**
	 * @return the sortingFields
	 */
	public SortingFields getSortingFields() {
		return sortingFields;
	}

	/**
	 * @param sortingFields
	 *            the sortingFields to set
	 */
	public void setSortingFields(SortingFields sortingFields) {
		this.sortingFields = sortingFields;
	}

	/**
	 * @return the favoriteSortingFields
	 */
	public SortingFields getFavoriteSortingFields() {
		return favoriteSortingFields;
	}

	/**
	 * @param favoriteSortingFields
	 *            the favoriteSortingFields to set
	 */
	public void setFavoriteSortingFields(SortingFields favoriteSortingFields) {
		this.favoriteSortingFields = favoriteSortingFields;
	}

	/**
	 * Create Quick Links data at run time. Since content is static so the data
	 * will be generated only once throughout the application TO DO : Needs to
	 * update the icons corresponding to the content.
	 */

	/**
	 * @return the myTeamGroupId
	 */
	public long getMyTeamGroupId() {
		return myTeamGroupId;
	}

	/**
	 * @return the myTeamGroupName
	 */
	public String getMyTeamGroupName() {
		return myTeamGroupName;
	}

	/**
	 * @return the quickLinksMenuOpen
	 */
	public boolean isQuickLinksMenuOpen() {
		return quickLinksMenuOpen;
	}

	/**
	 * @param quickLinksMenuOpen
	 *            the quickLinksMenuOpen to set
	 */
	public void setQuickLinksMenuOpen(boolean quickLinksMenuOpen) {
		this.quickLinksMenuOpen = quickLinksMenuOpen;
	}

	public int getNotificationCount() {
		return notificationCount;
	}


	public void setNotificationCount(int notificationCount) {
		this.notificationCount = notificationCount;
	}


	public List<MortgageBankerResponse> getBankerListResponse() {
		return bankerListResponse;
	}

	public void setBankerListResponse(List<MortgageBankerResponse> list) {
		this.bankerListResponse = list;
	}

	/**
	 * Checks the application's mode.
	 * @return true if the application is running in development mode 
	 * else false for release mode. default value is false 
	 */
	public boolean isDevelopmentMode() {
		try {
    		PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
    		int flags = packageInfo.applicationInfo.flags; 
    		if ((flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0) {
    			// development mode
    			return true;
    		} else {
    			// release mode
    			return false;
    		}
    	} catch(NameNotFoundException e) {
    		OpenHouseLog.e(getClass().getSimpleName(), "isDevelopmentMode() = " + e);
    		return false; // default false
    	}
	}

	public void setUseDevelopmentEnvironment(boolean useDevelopmentEnvironment) {
		this.useDevelopmentEnvironment = useDevelopmentEnvironment;
	}

	public boolean isUseDevelopmentEnvironment() {
		return useDevelopmentEnvironment;
	}
	
	/**
	 * Default false
	 * @return
	 */
	public boolean isCoreLogicQAEnvironment() {
		return PreferenceManager.getBoolean(this, Keys.PreferenceKeys.PREF_IS_CORE_LOGIC_QA_ENV, true);
	}

	public void setCoreLogicQAEnvironment(boolean isCoreLogicQAEnvironment) {
		PreferenceManager.put(this, Keys.PreferenceKeys.PREF_IS_CORE_LOGIC_QA_ENV, isCoreLogicQAEnvironment);
	}

	/**
	 * Default false
	 * @return
	 */
	public boolean isChaseQAEnvironment() {
		return PreferenceManager.getBoolean(this, Keys.PreferenceKeys.PREF_IS_CHASE_QA_ENV, true);
	}

	public void setChaseQAEnvironment(boolean isChaseQAEnvironment) {
		PreferenceManager.put(this, Keys.PreferenceKeys.PREF_IS_CHASE_QA_ENV, isChaseQAEnvironment);
	}
	
	/**Chase Difference Environment service Picker **/
	public int getChaseServiceEnvironment() {
		return chaseEnvironmentPickerPosition;
	}

	public void setChaseServiceEnvironment(int environmentPickerPosition) {
		this.chaseEnvironmentPickerPosition=environmentPickerPosition;
	}
	/**Chase Analytic Difference Environment service Picker **/
	public int getChaseAnalyticServiceEnvironment() {
		return chaseAnalyticEnvironmentPickerPosition;
	}

	public void setChaseAnalyticServiceEnvironment(int analyticEnvironmentPickerPosition) {
		this.chaseAnalyticEnvironmentPickerPosition=analyticEnvironmentPickerPosition;
	}
	/**
	 * Default false
	 * @return
	 */
	public boolean isAnalyticsQAEnvironment() {
		return PreferenceManager.getBoolean(this, Keys.PreferenceKeys.PREF_IS_ANALYTICS_QA_ENV, true);
	}

	public void setAnalyticsQAEnvironment(boolean isAnalyticsQAEnvironment) {
		PreferenceManager.put(this, Keys.PreferenceKeys.PREF_IS_ANALYTICS_QA_ENV, isAnalyticsQAEnvironment);
	}
	/** Added for scrolling issue to maintain the listitem position **/
	public int getFirstVisibleItem() {
		return firstVisibleItem;
	}
	
	public int getListingOffset() {
		return listingOffset;
	}

	public void setFirstVisibleItem(int firstVisibleItem, int offset) {
		this.firstVisibleItem = firstVisibleItem;
		this.listingOffset = offset;
	}

	public int getFavoriteItemClicked() {
		return favoriteItemClicked;
	}

	public void setFavoriteItemClicked(int favoriteItemClicked) {
		this.favoriteItemClicked = favoriteItemClicked;
	}

	/** Added for upgrade check **/
	/**
	 * @return the upgradeCheck
	 */
	public boolean isUpgradeCheck() {
		return upgradeCheck;
	}

	/**
	 * @param upgradeCheck
	 *            the upgradeCheck to set
	 */
	public void setUpgradeCheck(boolean upgradeCheck) {
		this.upgradeCheck = upgradeCheck;
	}

    public Research getResearch() {
        if (PreferenceManager.getBoolean(this, Keys.PreferenceKeys.PREF_IS_RESEARCH_DATA_AVAIL,
                false)) {
            Research research = new Research();
            research.setAveragePrice(PreferenceManager.getString(this,
                    Keys.PreferenceKeys.PREF_RESEARCH_AVG_PRICE));
            research.setAverageSqFt(PreferenceManager.getString(this,
                    Keys.PreferenceKeys.PREF_RESEARCH_AVG_SQFT));
            research.setPricePerSqft(PreferenceManager.getString(this,
                    Keys.PreferenceKeys.PREF_RESEARCH_PRICE_PER_SQFT));
            return research;
        } else {
        	OpenHouseLog.e("OpenHouseApplication", "@@Research getResearch() not available");
            return null;
        }
    }

    public void setResearch(Research research) {
        if (research != null) {
            PreferenceManager.put(this, Keys.PreferenceKeys.PREF_IS_RESEARCH_DATA_AVAIL, true);
            PreferenceManager.put(this, Keys.PreferenceKeys.PREF_RESEARCH_AVG_PRICE,
                    research.getAveragePrice());
            PreferenceManager.put(this, Keys.PreferenceKeys.PREF_RESEARCH_AVG_SQFT,
                    research.getAverageSqFt());
            PreferenceManager.put(this, Keys.PreferenceKeys.PREF_RESEARCH_PRICE_PER_SQFT,
                    research.getPricePerSqft());
        } else {
            PreferenceManager.put(this, Keys.PreferenceKeys.PREF_IS_RESEARCH_DATA_AVAIL, false);
            PreferenceManager.put(this, Keys.PreferenceKeys.PREF_RESEARCH_AVG_PRICE, "0");
            PreferenceManager.put(this, Keys.PreferenceKeys.PREF_RESEARCH_AVG_SQFT, "0");
            PreferenceManager.put(this, Keys.PreferenceKeys.PREF_RESEARCH_PRICE_PER_SQFT, "0");
        }
    }
    
    public void updateNotificationBadge() {
        updateNotificationCount();
    }
    
    
    private void updateNotificationCount() {
        int value = PreferenceManager.getInt(getBaseContext(),
                Keys.PreferenceKeys.PREF_NOTIFICATION_COUNT);
        int totalCount = NotificationDAO.getNumberOfTotalNotifications();
        boolean isChecked = PreferenceManager.getBoolean(getBaseContext(),
                Keys.PreferenceKeys.PREF_IS_NOTIFICATION_CHECKED);
        if (value <= 0) {
            PreferenceManager.put(getBaseContext(), Keys.PreferenceKeys.PREF_NOTIFICATION_COUNT,
                    totalCount);
            PreferenceManager.put(getBaseContext(), Keys.PreferenceKeys.PREF_NEW_NOTIFICATION,
                    totalCount);
        }
        if (value != 0 && value < totalCount) {
            if (value >= 0 && isChecked) {
                PreferenceManager.put(getBaseContext(), Keys.PreferenceKeys.PREF_NEW_NOTIFICATION,
                        totalCount - value);
            } else if (!isChecked) {
                PreferenceManager.put(getBaseContext(), Keys.PreferenceKeys.PREF_NEW_NOTIFICATION,
                        totalCount);
            }
            PreferenceManager.put(getBaseContext(), Keys.PreferenceKeys.PREF_IS_NOTIFICATION_CHECKED,
                    false);
            PreferenceManager.put(getBaseContext(), Keys.PreferenceKeys.PREF_NOTIFICATION_COUNT,
                    totalCount);
        }
    }

    
    public boolean isRefreashNotificationsMenu() {
        return refreashNotificationsMenu;
    }

    
    public void setRefreashNotificationsMenu(boolean refreashNotificationsMenu) {
        this.refreashNotificationsMenu = refreashNotificationsMenu;
    }

    
    public boolean isFilterMaxPriceChoosen() {
        return filterMaxPriceChoosen;
    }

    
    public void setFilterMaxPriceChoosen(boolean filterMaxPriceChoosen) {
        this.filterMaxPriceChoosen = filterMaxPriceChoosen;
    }
}
